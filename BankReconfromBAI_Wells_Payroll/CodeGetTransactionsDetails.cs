﻿using System;
using System.ComponentModel;


namespace BankReconfromBAI_Wells_Payroll
{
    class CodeGetTransactionDetails : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        //properties
        public string Rec88 { set; get; }
        public string AcctUnit { set; get; }
        public string DistAcct { set; get; }
        public string SubAcct { set; get; }
        public string BaiCode { set; get; }

        //method
        public void UnScramble88()
        {
            int strtOfLast = 0;
            string tmp = string.Empty;
            try
            {
                DistAcct = "99999";
                switch (BaiCode)
                {
                    case "169":     // Misc ACH Credit
                        if (Rec88.ToUpper().Contains("ACH RETURNS")
                            || Rec88.ToUpper().Contains("ACH REVERSAL")
                            || Rec88.ToUpper().Contains("ACH REJECT")
                            )
                        {
                            DistAcct = "20146";
                            AcctUnit = "900000";
                        }
                        break;
                    case "206":     // Book Transfer Credit
                        if (Rec88.ToUpper().Contains("WT SEQ"))
                        {
                            DistAcct = "10099";
                            AcctUnit = "900000";
                        }
                        break;
                    //case "195":
                    //    break;
                    //case "277":
                    //    break;
                    case "301":     // Commercial Deposit Credit
                        if (Rec88.ToUpper().Contains("OVER THE COUNTER DEPOSIT"))
                        {
                            DistAcct = "99999";
                            AcctUnit = "900000";
                        }
                        break;
                    case "469":     // Outgoing Money Transfer Debit
                        DistAcct = "20200";
                        SubAcct = string.Empty;
                        // AU??????
                        strtOfLast = Rec88.LastIndexOf("/88,", StringComparison.Ordinal);
                        tmp = Rec88.Substring(strtOfLast + 4);
                        try
                        {
                            if (Rec88.ToUpper().Contains("VA DEPT TAXATION")
                                || Rec88.ToUpper().Contains("AL-DEPT OF REV")
                                //|| Rec88.ToUpper().Contains("ESC OF NC E-CHECK")      per Sabrina 7/27/2017
                                || Rec88.ToUpper().Contains("GEORGIA ITS TAX GA TX PYMT")
                                || Rec88.ToUpper().Contains("IRS USATAXPYMT")
                                || Rec88.ToUpper().Contains("NC DEPT REVENUE TAX PYMT")
                                || Rec88.ToUpper().Contains("SC DEPT REVENUE DEBIT")
                                || Rec88.ToUpper().Contains("STATE OF LOUISIA")
                                )
                            {
                                DistAcct = "20200";
                                AcctUnit = "900000";
                            }
                            else if (Rec88.ToUpper().Contains("ACH ORIGINATION - ZAX INC"))
                            {
                                DistAcct = "20010";
                                AcctUnit = "900000";
                            }
                            else if (Rec88.ToUpper().Contains("ACH RETURNS - ZAX INC"))
                            {
                                DistAcct = "20146";
                                AcctUnit = "900000";
                            }
                            else if (Rec88.ToUpper().Contains("TRANSAMERICA L&A CONTRIBUTE"))
                            {
                                DistAcct = "20180";
                                AcctUnit = "900000";
                            }
                            else if (Rec88.ToUpper().Contains("IRS USATAXPYMT"))
                            {
                                DistAcct = "20150";
                                AcctUnit = "900000";
                            }
                            else if (Rec88.ToUpper().Contains("GA DEPT OF LABOR UI TAX PMT")       //per Sabrina 7/27/2017
                                || Rec88.ToUpper().Contains("SCDEW TAXPAYMENT DEBIT")
                                || Rec88.ToUpper().Contains("FLA DEPT REVENUE CUT ")      
                                || Rec88.ToUpper().Contains("ESC OF NC E-CHECK")
                                || Rec88.ToUpper().Contains("DEPT OF LABOR UC_TAX")
                                || Rec88.ToUpper().Contains("STATE OF LOUISIA LA UI TAX ")
                                || Rec88.ToUpper().Contains("VA. EMPLOY COMM UITAX PAID")
                                || Rec88.ToUpper().Contains("TN Dept of Labor SUTA")
                                )
                            {
                                DistAcct = "20171";
                                AcctUnit = "900000";
                            }
                        }
                        catch (Exception)
                        {
                            AcctUnit = tmp.Trim();
                        }
                        break;
                    case "475":     // Check Paid Debit
                        if (Rec88.ToUpper().Contains("CHECK"))
                        {
                            DistAcct = "20190";
                            AcctUnit = "900000";
                        }
                        break;
                    case "495":     // Money Transfer Debit
                        if (Rec88.ToUpper().Contains("WT FED"))
                        {
                            DistAcct = "10300";
                            AcctUnit = "900000";
                        }
                        break;
                    case "698":     // Misc Fees Debit
                        if (Rec88.ToUpper().Contains("CLIENT ANALYSIS"))
                        {
                            DistAcct = "51700";
                            AcctUnit = "900000";
                        }
                        break;
                    //case "699":
                    //    break;
                    default:
                        break;
                }
                // clean up au just in case...
                tmp = AcctUnit.Replace("#", "");
                tmp = tmp.Replace("ZAXBY S", "");
                tmp = tmp.Replace("ZXBYS", "");
                tmp = tmp.Replace("ZAXBYS", "");
                tmp = tmp.Replace("ZAX", "");
                tmp = tmp.Replace("INV", "");
                tmp = tmp.TrimStart('0');
                tmp = tmp.Replace("O", "0");
                tmp = tmp.Trim();
                using (CodeSQL clsSql = new CodeSQL())
                {
                    switch (tmp.Length)
                    {
                        case 1:
                            tmp = "0010" + tmp;
                            tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                            break;
                        case 2:
                            tmp = "001" + tmp;
                            tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                            break;
                        case 3:
                            tmp = "00" + tmp;
                            tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                            break;
                        case 4:
                            //tmp = clsSqlCe.ExecuteScalar($"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'");
                            tmp = tmp + "00";
                            break;
                        case 5:
                            tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                            break;
                        default:
                            break;
                    }
                }
                AcctUnit = tmp;
            }
            catch (Exception)
            {
                AcctUnit = tmp;
            }
        }
    }
}
